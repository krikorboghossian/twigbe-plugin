<?php
/* The Admin UI for global Options
 *
 *
 * @since      1.0.0
 * @package    Twigbe
 * @subpackage Plugin_Name/includes
 * @author     Sotiris Kyritsis, Krikor Boghossian
 */
class Twigbe_Admin_UI {

    public function __construct() 
    {
        add_action( 'admin_init', array($this, 'twigbe_register_settings' ));
    }

    public function render()
    {
        $this->twigbe_global_custom_options();
    }

  	/**
	 * Register the settings about the 
	 * a) Click costs - 2 numerical fields
	 * b) Click limits for emails - 2 numerical fields
	 * c) Email texts - 3 emails wysuwyg
	 */
	public function twigbe_register_settings()
	{
		add_option("price_per_click", "", "", "yes");
		add_option("price_per_click_for_payout", "", "", "yes");
		add_option("clicks_limit_a", "", "", "yes");
		add_option("clicks_limit_b", "", "", "yes");
        add_option("clicks_limit_cashout", "", "", "yes");

		add_option("mail_options_first_notification", "", "", "yes");
		add_option("mail_options_second_notification", "", "", "yes");
		add_option("mail_options_last_notification", "", "", "yes");
	
		// Register settings that this form is allowed to update
		register_setting('twigbe_settings', 'price_per_click');
		register_setting('twigbe_settings', 'price_per_click_for_payout');
		register_setting('twigbe_settings', 'clicks_limit_a');
		register_setting('twigbe_settings', 'clicks_limit_b');
		register_setting('twigbe_settings', 'clicks_limit_cashout');

		register_setting('twigbe_settings', 'mail_options_first_notification');
		register_setting('twigbe_settings', 'mail_options_second_notification');
		register_setting('twigbe_settings', 'mail_options_last_notification');
	}

    /**
	 * Set the global params for the payout and cost of clicks
	 * through the admin plugin instead of the theme
	 */
	private function twigbe_global_custom_options()
	{
	?>
	    <?php if (!current_user_can('manage_options'))
			wp_die(__("You don't have access to this page"));
		?>
		<div class="wrap">
            <?php $this->twigbe_admin_header(); ?> 
          
            <form method="post" action="options.php">
				<?php wp_nonce_field('update-options') ?>
				<?php settings_fields('twigbe_settings'); ?>

				<p><strong>Price For Click:</strong><br />
					<input type="text" name="price_per_click" size="45" value="<?php echo esc_attr( get_option('price_per_click')); ?>" />
				</p>

				<p><strong>Price For Payout Click:</strong><br />
					<input type="text" name="price_per_click_for_payout" size="45" value="<?php echo esc_attr( get_option('price_per_click_for_payout') ); ?>" />
				</p>
				
                <?php $this->twigbe_global_custom_options_mail_limits(); ?>
                <?php $this->twigbe_global_custom_options_mails(); ?>

				<?php submit_button('save', 'primary sub-button', 'submit', true); ?>

				<input type="hidden" name="action" value="update" />
				<input type="hidden" name="page_options" value="price_per_click" />
				<input type="hidden" name="page_options" value="price_per_click_for_payout" />
			</form>
		</div>
	<?php
	}

    /**
     * Pure presentational
     */
	private function twigbe_admin_header()
    {
        ?>
            <div class="wrap">
                <h1>Twigbe Global settings.</h1>
            </div>
        <?php
    }

    /**
     * The limits where the mails will be triggered
     */
	private function twigbe_global_custom_options_mail_limits()
    { 
        ?>
            <p><strong>Όριο για cashout:</strong><br />
                <input type="text" name="clicks_limit_cashout" size="45" value="<?php echo esc_attr( get_option('clicks_limit_cashout') ); ?>" />
            </p>

            <p><strong>Fist level for remaining clicks email:</strong><br />
                <input type="text" name="clicks_limit_a" size="45" value="<?php echo esc_attr( get_option('clicks_limit_a') ); ?>" />
            </p>

            <p><strong>Second level for remaining clicks email:</strong><br />
                <input type="text" name="clicks_limit_b" size="45" value="<?php echo esc_attr( get_option('clicks_limit_b') ); ?>" />
            </p>


			<input type="hidden" name="page_options" value="clicks_limit_a" />
            <input type="hidden" name="page_options" value="clicks_limit_b" />
            <input type="hidden" name="page_options" value="clicks_limit_cashout" />
        <?
    }

    /**
     * Get the text which will be sent to the vendor 
     * when he is running out of clicks
     */
	private function twigbe_global_custom_options_mails()
    {
        ?>
            <div class="wrap email">
                <strong>Few clicks:</strong><br />
                <?php
                    // Few Clicks remain
                    $field_value = esc_attr( get_option('mail_options_first_notification') );
                    $field_name  = 'mail_options_first_notification';
                    
                    wp_editor( $field_value, $field_name ); 
                ?>
				<br /><br />

                <strong>Fewer clicks:</strong><br />
                <?php 
                    // Even fewer clicks remain
                    $field_value = esc_attr( get_option('mail_options_second_notification') );
                    $field_name  = 'mail_options_second_notification';
                    
                    wp_editor( $field_value, $field_name ); 
                ?>
				<br /><br />


                <strong>Out of clicks:</strong><br />
                <?php
                    // No clicks remain
                    $field_value = esc_attr( get_option('mail_options_last_notification') );
                    $field_name  = 'mail_options_last_notification';
                    
                    wp_editor( $field_value, $field_name ); 
                ?>
                
                <input type="hidden" name="page_options" value="mail_options_first_notification" />
                <input type="hidden" name="page_options" value="mail_options_second_notification" />
                <input type="hidden" name="page_options" value="mail_options_last_notification" />
            </div>
        <?php
    }
}