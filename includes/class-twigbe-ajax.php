<?php
/**
 * Used for managing the frontend requests
 */
class Twigbe_Ajax {

    public function __construct()
    {
        add_action( 'wp_ajax_fav_images', array($this, 'fav_images' ));
        add_action( 'wp_ajax_fav_images_delete', array($this, 'fav_images_delete' ));
        add_action( 'wp_ajax_filter_posts_for_influencer', array($this, 'filter_posts_for_influencer' ));
    }
    
    /**
     * Toggle the value for front end filtering
     * Change the global and refresh
     */
    public function filter_posts_for_influencer() : void
    {

        // Some security
        // https://codex.wordpress.org/AJAX_in_Plugins
        if ( !wp_verify_nonce( $_POST['nonce'], 'filter_posts_for_influencer_nonce')) {
            exit('Wrong nonce');
        }

        global $filter_posts;
        $filter_posts = !$filter_posts;
        
        setcookie('filter_posts_by_vendor', $filter_posts, time() + (86400 * 30), "/"); // 86400 = 1 day
    }

    /**
     * Handle the AJAX call made in the frontend
     * Update the ACF meta
     */
    public function fav_images() : void
    {

        // Some security
        // https://codex.wordpress.org/AJAX_in_Plugins
        if ( !wp_verify_nonce( $_POST['nonce'], 'fav_images_nonce')) {
            exit('Wrong nonce');
        }

        $image        = intval( $_POST['image'] );
        //@todo - Sanitize this!!!
        $desc         = esc_html($_POST['description']);
        $current_user = wp_get_current_user();
        $user         = intVal($current_user->ID);
        $meta         = get_user_meta($user);

        $this->verifyUser($user, $image, $meta);
        global $wpdb; // this is how you get access to the database

        $existingSelections = $meta['products'];
        $metaToSave = empty($existingSelections)
            ? $image
            : $existingSelections[0]. ',' . $image;

        // Add the twig to the user's meta
        $this->updateUserPreferences($user, $metaToSave);
        // Add the review to the twig
        $this->add_description_to_post($image, $user, $desc);
        // Mark the selections both ways post -> influencers
        $this->handlePostAddition($image, $user);

        wp_die(); // this is required to terminate immediately and return a proper response
    }

    /**
     * Add the product review
     */
    private function add_description_to_post(int $post_id, int $user_id, string $desc) : void
    {
        $description = array(
            'user_id' => $user_id,
            'description' => $desc
        );

        add_post_meta( $post_id, 'product_review', $description, false );
    }

    /**
     * Remove the review after locating it amongst other
     * reviews
     */
    private function remove_description_from_post(int $post_id, int $user_id) : void
    {

        $metas = get_post_meta( $post_id, 'product_review', false );
        $description = null;

        foreach ($metas as $meta)
        {
            if ($meta['user_id'] === $user_id)
            {
                $description = $meta;
            }
        }

        if($description)
        {
            delete_post_meta($post_id, 'product_review', $description);
        }
    }

    /**
     * Delete the selected image
     */
    public function fav_images_delete() : void
    {
        // https://codex.wordpress.org/AJAX_in_Plugins
        if ( !wp_verify_nonce( $_POST['nonce'], 'fav_images_delete_nonce')) {
            exit('Wrong nonce');
        }

        $image        = intval( $_POST['image'] );
        $current_user = wp_get_current_user();
        $user         = $current_user->ID;
        $meta         = get_user_meta($user);

        $this->verifyUser($user, $image, $meta);

        $existingSelections = $meta['products'][0];
        $posts              = explode(',', $existingSelections);
        $key                = array_search($image, $posts);

        // Remove the existing element
        unset($posts[$key]);
        $posts = implode(",", $posts);

        // And update the selections
        $this->updateUserPreferences($user, $posts);
        // Remove the review
        $this->remove_description_from_post($image, $user);
        // Un-mark the twig as selected
        $this->handlePostRemoval($image, $user);

        wp_die();
    }

    /**
     * Verify that the user belongs to the correct group
     */
    public function verifyUser(int $user, int $image, array $meta) : void
    {
        $isInfluencer = ($meta['influencer'][0] === '1');

        if(empty($user) || empty($image)){
            exit('Wrong user or image');
        }

        if( !Twigbe_Common::is_user_influencer( $user ) ){
            exit('Wrong user level');
        }
    }

    protected function updateUserPreferences(int $user, string $value) : void
    {
        update_user_meta($user, 'products', $value);
        echo 'OK';
    }

    /**
     * Update the Post (twigbe) to the link (or remove the link)
     * the influencer as well
     */
    private function handlePostAddition(int $image, int $user) : void
    {
        $meta = get_post_meta($image);

        $existingSelections = $meta['influencer_ids'][0];
        echo('SELCTIONS '. $existingSelections);
        $metaToSave = empty($existingSelections) ?
            $user :
            $existingSelections[0]. ',' . $user;

        $this->updatePostMeta($image, $metaToSave);
    }

    /**
     * Update the Post (twigbe) to the link (ore remove the link)
     * the influencer as well
     */
    private function handlePostRemoval(int $image, int $user) : void
    {
        $meta               = get_post_meta($image);
        $existingSelections = $meta['influencer_ids'][0];
        $user_array         = explode(',', $existingSelections);
        $key                = array_search($image, $user_array);

        // Remove the existing element
        unset($user_array[$key]);
        $user_array = implode(",", $user_array);

        $this->updatePostMeta($image, $user_array);
    }

    /**
     * Update the post and add/remove the id from the influencers
     * array list
     */
    private function updatePostMeta(int $image, string $user_array) : void
    {
        update_post_meta($image, 'influencer_ids', $user_array);
        echo 'POST '. $image . 'META UPDATED WITH' . $user_array;
    }
}
?>
