<?php
/**
 * Used for managing the frontend requests
 * to transfer the clicks between two registered users
 */
class Twigbe_Points_System extends Twigbe_Click_Counter {

    public function __construct()
    {
        add_action( 'wp_ajax_transferPoints', array($this, 'transferPoints' ));
        register_activation_hook( __FILE__, array($this, 'createPointsDB') );
    }

    /**
     * Track the points which where transfered to a user 
     * from another user
     * 
     * @uses user: the id of the giver
     * @uses target: the if of the receiver
     * @uses ip
     * @uses timestamp
     */
    private function createPointsDB()
    {
        global $wpdb;
        $tablename = $wpdb->prefix.'point_transfer_data';

        $click_data_table = $wpdb->get_results("SHOW TABLES LIKE '".$tablename."'" , ARRAY_N);
        
        if(empty($click_data_table))
        {
            $charset_collate = $wpdb->get_charset_collate();
            $create_table_sql = "CREATE TABLE $tablename (
                id BIGINT(50) NOT NULL AUTO_INCREMENT, 
                ip VARCHAR(255) NOT NULL, 
                timestamp INT NOT NULL,
                user SMALLINT NOT NULL, 
                target SMALLINT NOT NULL, 
                balance SMALLINT NOT NULL, 
                points SMALLINT NOT NULL, 
                PRIMARY KEY (id), 
                UNIQUE (id)
            ) $charset_collate;";

            $wpdb->query( $create_table_sql );
            $wpdb->show_errors();
            $wpdb->flush();

            if(empty($wpdb->get_results("SHOW TABLES LIKE '".$tablename."'" , ARRAY_N))) 
            {
                echo '[ERROR]:: Could not create the database table for the clicks.';
            }
        }

        wp_die();
    }

    /**
     * Transfer the points from user to another
     * Used via AJAX in the frontend
     */
    public function transferPoints()
    {

        if ( !wp_verify_nonce( $_POST['nonce'], 'transferPoints_nonce')) 
        {
            exit('Wrong nonce');
        }

        $user   = $_POST['user'];
        $target = $_POST['target'];
        $points = $_POST['points'];
        $hash   = $_POST['hash'];
        $thash  = $_POST['thash'];

        $user_points = get_user_meta($user, 'points_awarded', true);
        $user_hash  = get_user_meta($user, 'points_hash', true);
        $target_hash  = get_user_meta($target, 'points_hash', true);

        $check_user = get_user_by('id', $user);
        
        // Some basic checking        
        if(empty($check_user) || !$check_user)
        {
            exit('wrong user');
        }

        if(intVal($points) > intVal($user_points))
        {
            exit('not enough points');
        }

        if($hash != $user_hash)
        {
            exit('Wrong user');
        }

        if($thash != $target_hash)
        {
            exit('Wrong target user');
        }

        // Do it procedurally in order to avoid extra SQL queries
        $target_user = get_user_by('id', $target);

        if(empty($target_user) || !$target_user)
        {
            exit('Invalid user');
        }

        // Do it procedurally in order to avoid extra SQL queries
        $meta = get_user_meta($target_user);

        // if($meta['influencer'][0] === '1' || $meta['vendor'][0] === '1')
        if( Twigbe_Common::is_user_vendor_or_influencer( $target_user  ) )
        {
            exit('You cannot supply points to this user');
        }

        $this->awardPointsFromTransfer($points, $target);

        // Deduct the points from the user
        $remaining_clicks = intVal($user_points) - intVal($points) ;
        $this->removePoints($user, $remaining_clicks);

        // Finally record it to the database
        $ip = Twigbe_Common::get_user_ip();
        $this->recordPointsTransfer($user, $target, $points, $ip, $remaining_clicks);
    
        wp_send_json(
            array(
                'user'        => $user, 
                'target_user' => $target, 
                'points'      => $points, 
                'points_left' => $remaining_clicks
            )
        );
        wp_die();
    }

    private function awardPointsFromTransfer(int $points, int $target)
    {
        parent::$target = $target;
        parent::$points = $points;
        parent::awardPointToUser();
    }

    /**
     * Remove the points and create a new hash
     * @param: $user - the id
     * @param: $points - int
     */
    private function removePoints(int $user, int $points)
    {
        update_user_meta($user, 'points_awarded', intVal($points));

        // Re apply the hash
        $random_hex = bin2hex(random_bytes(18));
        update_user_meta( $user, 'points_hash', $random_hex);
    }

    /**
     * Record the transfer of the points into the table cell
     * @param $user: from
     * @param $target: to
     * @param $points: int
     * @param $ip
     * @param $balance: ip - the remaining points of the user
     * at that given time
     */
    private function recordPointsTransfer(int $user, int $target, int $points, string $ip, int $balance)
    {
        global $wpdb;
        $tablename = $wpdb->prefix.'point_transfer_data';

        $date      = new DateTime();
        $timestamp = $date->getTimestamp();

        $data = array(
            'balance'   => intval($balance), 
            'ip'        => $ip,
            'timestamp' => $timestamp,
            'target'    => intval($target), 
            'user'      => intval($user),
            'points'    => intval($points)
        );

        $wpdb->insert($tablename, $data);
    }


    /**
     * Get the x top users with points
     */
    public static function getLeaderBoard(int $limit)
    {

    }
}