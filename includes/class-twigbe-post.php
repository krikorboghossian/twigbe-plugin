<?php
/**
 * Used for managing the frontend requests
 * in order to autopost to Peepso
 */
class Twigbe_AutoPost {

    public function __construct()
    {
        add_action( 'wp_ajax_post_to_peepso', array($this, 'post_to_peepso' ));
    }

    public function post_to_peepso()
    {
        $current_user = wp_get_current_user();
        $user_id    = $current_user->ID;
        
        $image_id   = $_POST['image'];
        $user_text  = $_POST['user_text'];
        $influencer = $_POST['influencer'];
        $original   = $_POST['original_id'];

        // Some security
        // https://codex.wordpress.org/AJAX_in_Plugins
        if ( !wp_verify_nonce( $_POST['nonce'], 'post_to_peepso_nonce'))
        {
            exit('Wrong nonce');
        }

        if($user_id === 0)
        {
            exit('Wrong user');
        }

        $meta = get_user_meta($user_id);

        //both for post_title and post_name
        $title     = $this->construct_title($user_id);
        $guid      = $this->construct_guid($user_id);
        $content   = $this->construct_content($user_id, $image_id, $user_text);

        // Create post object
        $my_post = array(
          'post_title'    => $title,
          'post_name'     => $title,
          'guid'          => $guid,
          'post_content'  => $content,
          'post_status'   => 'publish',
          'post_author'   => $user_id,
          'post_type'     => 'peepso-post',
          'meta_input'    => array(
              'peepso_human_friendly '       => wp_strip_all_tags( $content, true ),
              'peepso_hashtags_done'         => 1,
              '_peepso_display_link_preview' => 1,
              '_peepso_gallery_images'       => $image_id,
              '_peepso_influencer'           => $influencer,
              '_peepso_original_post'        => $original
          )
        );

        // Insert the post into the database
        $post_ID = wp_insert_post( $my_post );
        $this->add_to_peepso_db($user_id, $post_ID);

        wp_die();
    }

    /**
     * Construct the excerpt / content for the peepso post
     * The images are added in metas
     */
    private function construct_content($user_id, $image_id, $user_text)
    {
        if(!empty($user_text))
        {
            return 'I just added this amazing product to my collection - ' . sanitize_text_field( $user_text );
        }
        else{
            return 'I just added these amazing products to my collection';
        }
    }

    /**
     * Specific to Peepso's format
     * user_id (to) - user_id (to) - 165909897
     */
    private function construct_title($id)
    {
        $timestamp = round(microtime(true));
        return $id.'-'.$id.'-'.$timestamp;
    }

    /**
     * Specific to Peepso's format
     * Example:  https://twigbe.com/dev/peepso-post/1-1-1638913684/
     */
    private function construct_guid($id)
    {
        return site_url().'/peepso-post/'.$this->construct_title($id).'/';
    }

    /**
     * Unlike WP_Post peepso needs to add data to a custom table as wel
     *
     * Table format
     * tg01bbe_peepso_activities
     * act_owner_id => user id
     * act_external_id => $post_ID
     * act_module_id => 1
     * act_ip => ip
     * act_access => 10
     */
    private function add_to_peepso_db($user_id, $post_ID)
    {

        global $wpdb;

        $table = 'tg01bbe_peepso_activities';
        $ip    = Twigbe_Common::get_user_ip();

        $data  = array(
            'act_owner_id'    => $user_id,
            'act_external_id' => $post_ID,
            'act_module_id'   => 1, // the activity module
            'act_ip'          => $ip,
            'act_access'      => 10
        );

        $wpdb->insert( $table, $data);
    }
}
