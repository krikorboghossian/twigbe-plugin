<?php
/**
 * Used for managing the frontend requests
 * and updating the click data
 */
class Twigbe_Click_Counter {

    protected static $target = null;
    protected static $points = 0;

    public function __construct()
    {
        add_action( 'wp_ajax_getUserAndCheckClick', array($this, 'getUserAndCheckClick' ));
        add_action( 'wp_ajax_nopriv_getUserAndCheckClick', array($this, 'getUserAndCheckClick' ));
        
        // Create the necessary two database tables
        register_activation_hook( __FILE__, array($this, 'createDB') );
    }

    /**
     * Create the table for the click data
     * 
     * @uses $guid: unique identifier
     * @uses $referrer: the influencer
     * @uses $vendor: the vendor
     * @uses $url: the influencer's page
     * @uses ip
     * @uses timestamp
     */
    private function createDB() : void
    {
        global $wpdb;
        $tablename = $wpdb->prefix.'click_data';

        $click_data_table = $wpdb->get_results("SHOW TABLES LIKE '".$tablename."'" , ARRAY_N);
        
        if(empty($click_data_table))
        {
            $charset_collate = $wpdb->get_charset_collate();
            $create_table_sql = "CREATE TABLE $tablename (
                id BIGINT(50) NOT NULL AUTO_INCREMENT, 
                url VARCHAR(255) NOT NULL, 
                guid VARCHAR(255) NOT NULL, 
                ip VARCHAR(255) NOT NULL, 
                timestamp INT NOT NULL,
                refferer SMALLINT NOT NULL, 
                vendor SMALLINT NOT NULL, 
                PRIMARY KEY (id), 
                UNIQUE (id)
            ) $charset_collate;";

            $wpdb->query( $create_table_sql );
            $wpdb->show_errors();
            $wpdb->flush();

            if(empty($wpdb->get_results("SHOW TABLES LIKE '".$tablename."'" , ARRAY_N))) 
            {
                echo '[ERROR]:: Could not create the database table.';
            }
        }
    }

    /**
     * Get the initial request and then:
     * Get any results from the database with the same database and ip
     * If none store it and update the users' meta
     * if one or many exist select the newer and if it's older than x hours
     * then insert it and update the user <meta class="]">  
     * 
     * @param $guid: unique identifier
     * @param $referrer: the influencer
     * @param $vendor: the vendor
     * @param $url: the influencer's page
     */
    public function getUserAndCheckClick() : void
    {
        // Some security
        // https://codex.wordpress.org/AJAX_in_Plugins
        if ( !wp_verify_nonce( $_POST['nonce'], 'getUserAndCheckClick_nonce')) {
            exit('Wrong nonce');
        }

        $url      = $_POST['url'];
        $guid     = $_POST['guid'];
        $vendor   = intVal($_POST['vendor']);
        $pointsT  = $_POST['pp'];
        $ip       = Twigbe_Common::get_user_ip();

        // 0 tracks organic clicks
        // or clicks from posts where no influencer
        // has promoted this product
        $referrer = empty( $_POST['ref'] ) 
            ? 0
            : intVal($_POST['ref']);

        $user     = empty($user) 
            ? null
            : intVal($_POST['user']);

        $shouldInsert  = false;
        $existing_data = $this->getClickData($guid, $referrer, $vendor, $url, $ip);
        
        // There are rows from this IP and this url and guid
        if(!empty($existing_data))
        {

            // Check the timestamp and process it if it's newer than x hours
            $last_click = intval($existing_data[0]->timestamp);
            $date       = date_create();
            $timestamp  = date_timestamp_get($date);
            $interval   = $timestamp - $last_click;
            
            if($interval < 3600 * 12)
            {
                exit('[ERROR]:: Not so soon...');
            }
            else {
                // Second check for same guid and ip
                // since the URL can be tampered with
                $entries = $this->detectDuplicateEntriesByTheSameIp($guid,  $referrer, $vendor,  $url, $ip);
                
                if(!empty($entries))
                {
                    $last_entry = intval($entries[0]->timestamp);
                    $date       = date_create();
                    $timestamp  = date_timestamp_get($date);
                    $interval   = $timestamp - $last_entry;

                    // Half an hour between different clicks with a different 
                    // URL but the same GUID
                    if($interval < 3600 * 0.5)
                    {
                        exit('[ERROR]:: Not so soon...');
                    }
                }

                // OLD results do add it and update the meta
                // First time clicked
                // or > 12 hours from the last click
                // or > 0.5 hours from a click with the same guid
                $shouldInsert = true;
            }
        }
        else {
            // No results do add it and update the meta
            $shouldInsert = true;
        }
        // Finally update the users' and post's data
        if($shouldInsert){

            // Record the transaction
            $this->insertToDB($guid, $referrer, $vendor, $url, $ip);

            // Charge the Vendor
            $this->updateVendorData($vendor);

            // Add credit to the influencer - if any
            if($referrer !== 0)
            {
                $this->updateInfluencerData($referrer);
            }

            // Register the clicks to the post
            $this->updatePostClicks($guid);

            // Give that man a cookie
            if(!is_null($pointsT) && !empty($pointsT) && $pointsT != 0)
            {
                self::$target = $pointsT;
                self::$points = 1;
                self::awardPointToUser();

                wp_send_json(
                    array(
                        'user'         => $pointsT, 
                        'points'       => 1, 
                        'points_total' => get_user_meta( $pointsT, 'points_awarded', true )
                    )
                );
            }
        }

        wp_die();
    }

    
    /**
     * Detect if the same IP has requested the same guid within 
     * the last 30 minutes. So one cannot change the URL and 
     * try again.
     * 
     * If yes, then do not count the click.
     * 
     * @param $guid: unique identifier
     * @param $referrer: the influencer
     * @param $vendor: the vendor
     * @param $url: the influencer's page
     * @param $ip
     */
    private function detectDuplicateEntriesByTheSameIp(
        string $guid, int $referrer, int $vendor, string $url, string $ip) : array
    {
        global $wpdb;
        $tablename = $wpdb->prefix.'click_data';

        $results = $wpdb->get_results(" SELECT DISTINCT FROM $tablename 
            WHERE guid = '$guid'
                AND ip  = '$ip'
                ORDER BY timestamp DESC
            ");

        return $results;
        wp_die();
    }

    /**
     * Load any data for any previous clicks from the same ip 
     * 
     * @param $guid: unique identifier
     * @param $referrer: the influencer
     * @param $vendor: the vendor
     * @param $url: the influencer's page
     * @param $ip
     */
    private function getClickData(
        string $guid, int $referrer, int $vendor, string $url, string $ip) : array 
    {
        global $wpdb;
        $tablename = $wpdb->prefix.'click_data';

        $results = $wpdb->get_results(" SELECT * FROM $tablename 
            WHERE vendor = $vendor 
                AND refferer = $referrer
                AND guid = '$guid'
                AND ip  = '$ip'
                ORDER BY timestamp DESC
            ");

        return $results;
        wp_die();
    }

    /**
     * Add the click data to the database
     * 
     * @param $guid: unique identifier
     * @param $referrer: the influencer
     * @param $vendor: the vendor
     * @param $url: the influencer's page
     * @param $ip
     */
    private function insertToDB(string $guid, int $referrer, int $vendor, string $url, string $ip) : void
    {
        global $wpdb;
        $tablename = $wpdb->prefix.'click_data';

        $date      = new DateTime();
        $timestamp = $date->getTimestamp();

        $data = array(
            'url'       => $url, 
            'guid'      => $guid,
            'ip'        => $ip,
            'timestamp' => $timestamp,
            'refferer'  => intval($referrer), 
            'vendor'    => intval($vendor)
        );

        $wpdb->insert($tablename, $data);

        //wp_die(); // this is required to terminate immediately and return a proper response
    }

    /**
     * Decrease the clicks by one.
     * 
     * If the remaining count is zero then disable the user
     * Drive the respective notifications from there
     * 
     * @param $vendor: the user id
     * @param $value: the clicks
     */
    private function updateVendorData(int $vendor) : void
    {
        $clicks       = get_user_meta($vendor, 'clicks_consumed');
        $clicks_pack  = get_user_meta($vendor, 'clicks_packet') || [0];
        $final_clicks = intval($clicks[0]) - 1;
               
        update_user_meta($vendor, 'clicks_consumed', $final_clicks);
        
        $remaing_clicks = intval($clicks_pack[0]) - $final_clicks;
        
        $upper_limit = intVal(get_option('clicks_limit_a'));
        $lower_limit = intVal(get_option('clicks_limit_b'));

        if($final_clicks === 0)
        {
            update_user_meta($vendor, 'clicks_pack_active', false);
            $this->update_all('upbl', $vendor);
        }

        // Drive the notofications from here 
        $this->mail_flow($vendor, $final_clicks);
    }


    /**
     * Publish or unpublish all posts from a user
     * @param: $action - 'publish', 'unpublish'
     * @param: $user - id
     */
    public function update_all(string $action, int $user) : void 
    {
        $args = array(
            'post_type'      => 'collection',
            'posts_per_page' => -1,
            'author'         => $user
        );
        $products_array = get_posts($args);

        if (!empty($products_array))
        {
            foreach ($products_array as $product)
            {
                if($action === 'pbl')
                {
                    $product->post_status = 'publish';
                } else if($action === 'upbl') {
                    $product->post_status = 'draft';
                }
                wp_update_post($product);
            }
        }
    }
    

    /**
     * Based on the remaing clicks and the CMS settings
     * Drive the mail flow. Do not send an email
     * if the user_meta has been set. The flag is being reset in the functions.php file
     * 
     * @param: $user_id
     * @param: $clicks - Based on the number send the correct notification
     */
    private function mail_flow($user_id, $clicks) : void
    {
        $user = get_userdata( $user_id );
        
        $upper_limit = intVal( get_option('clicks_limit_a') );
        $lower_limit = intVal( get_option('clicks_limit_b') );

        $message     = null;
        $should_not_send_message = false;

        if($clicks === 0)
        {
            $message = esc_attr( get_option('mail_options_last_notification') );
        }
        else if($clicks <= $lower_limit){
            $should_not_send_message = get_user_meta($user_id, 'twi_second_mail_notification', true);
            $message = esc_attr( get_option('mail_options_second_notification') );

            // Flag the second notification as sent
            if(!$should_not_send_message)
            {
                update_user_meta($user_id, 'twi_second_mail_notification', true);
            }
        }
        else if($clicks > $lower_limit && $clicks <= $upper_limit)
        {
            $should_not_send_message = get_user_meta($user_id, 'twi_first_mail_notification', true);
            $message = esc_attr( get_option('mail_options_first_notification') );

            // Flag the first notification as sent
            if(!$should_not_send_message)
            {
                update_user_meta($user_id, 'twi_first_mail_notification', true);
            }
        }

        // Send the email if it hasn't been sent before
        if($message && !$should_not_send_message)
        {
            $email_to   = $user->data->user_email;
            $email_from = 'no-reply@twigbe.com';
            wp_mail( $email_to, '[Twigbe]:: You are low on clicks', $message );
        }
    }

    /**
     * Add one click to the post's meta
     * Get the id based on the guid
     */
    private function updatePostClicks(string $url) : void
    {
        $guid  = explode('&p=', $url);
        $id    = intVal($guid[1]);
        $views = get_post_meta($id, 'clicks_registered', true);

        if(!$views)
        {
            add_post_meta($id, 'clicks_registered', 1);
        }
        else {
            $clicks = intval($views) + 1;
            update_post_meta($id, 'clicks_registered', $clicks);
        }
    }

    /**
     * Autoincrement the clicks driven by one
     * 
     * @param $influencer: the user id
     * @param $value: the clicks
     */
    private function updateInfluencerData($influencer){
        $clicks       = get_user_meta($influencer, 'clicks_driven');

        $final_clicks = intval($clicks[0]) + 1;
        update_user_meta($influencer, 'clicks_driven', $final_clicks);
    }

    /**
     * Get a vendor's most clicked product
     * @param $id
     */
    public static function getMostViewedProductByVendor($id) : array
    {
        if(!$id){
            exit('No vendor provided');
        }
        global $wpdb;
        $tablename = $wpdb->prefix.'click_data';

        $query = $wpdb->get_results(" SELECT MAX(guid) 
            FROM $tablename
            WHERE vendor = $id");

        return $query;

        wp_die();
    }
    
    /**
     * Get a influencer's most clicked product
     * @param $id
     */
    public static function getMostViewedProductByInfluencer($id) : array
    {
        if(!$id){
            exit('No influencer provided');
        }

        global $wpdb;
        $tablename = $wpdb->prefix.'click_data';

        $query = $wpdb->get_results(" SELECT MAX(guid) 
            FROM $tablename
            WHERE influencer = $id");

        return $query;

        wp_die();
    }

    /**
     * Give that man a cookie
     */
    protected static function awardPointToUser() : void
    {
        $user_hash        = get_user_meta(self::$target, 'points_hash', true);
        $original_points  = get_user_meta(self::$target, 'points_awarded', true);

        // No hash means a new user
        if(!$user_hash || empty($user_hash))
        {
            $points_to_award = self::$points;
            update_user_meta( self::$target, 'points_awarded', self::$points);
        }
        else {
            $points_to_award = intVal($original_points) + intVal(self::$points);
            update_user_meta( self::$target, 'points_awarded', $points_to_award);
        }

        $random_hex = bin2hex(random_bytes(18));
        update_user_meta( self::$target, 'points_hash', $random_hex);
    }

    /**
     * Get the highest influencer income for a vendor
     * @param $vendor
     */
    public static function getHighestContributorForVendor(int $vendor) : array
    {
        if(!$vendor){
            exit('No params provided');
        }

        global $wpdb;
        $tablename = $wpdb->prefix.'click_data';

        $query = $wpdb->get_results(" SELECT MAX(refferer) 
            FROM $tablename
            WHERE vendor = $vendor
        ");

        return $query;
        wp_die();
    }

    /**
     *  Get the highest vendor income for an influencer
     *  @param $influencer
     */
    public static function getHighestProductVendorForInfluencer(int $influcer) : array
    {
        if(!$influcer){
            exit('No params provided');
        }

        global $wpdb;
        $tablename = $wpdb->prefix.'click_data';

        $query = $wpdb->get_results(" SELECT MAX(vendor) 
            FROM $tablename
            WHERE refferer = $influcer
        ");

        return $query;
        wp_die();
    }

}