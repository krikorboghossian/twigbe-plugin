<?php

/* The common values plugin class.
 *
 *
 * @since      1.0.0
 * @package    Twigbe
 * @subpackage Plugin_Name/includes
 * @author     Sotiris Kyritsis, Krikor Boghossian
 */
class Twigbe_Common {

     /**
     * Pretty standard stuff
     * Supports CDNs as well
     */
    public static function get_user_ip() : string
    {
        if ( ! empty( $_SERVER['HTTP_CLIENT_IP'] ) )
        {
            $ip = $_SERVER['HTTP_CLIENT_IP'];    
        } 
        elseif ( ! empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) )
        {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];   
        } 
        else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }


    /**
     * Get all the users which a given user is following
     * @param $user_id
     */
    public function get_user_followers(int $user_id) : array
    {
        global $wpdb;
        $tablename = $wpdb->prefix.'peepso_user_followers';

        $results = $wpdb->get_col("SELECT uf_passive_user_id
            FROM $tablename 
            WHERE uf_active_user_id = $user_id
            ");

        return $results;
        wp_die();
    }

        /**
     * Get all the users which a given user is following
     * @param $user_id
     */
    public static function get_user_following(int $user_id) : array
    {
        global $wpdb;
        $tablename = $wpdb->prefix.'peepso_user_followers';

        $results = $wpdb->get_col("SELECT uf_active_user_id
            FROM $tablename 
            WHERE uf_passive_user_id = $user_id");

        return $results;
        wp_die();
    }

    /**
     * Get all the friends of a vendor in the UI
     */
    public static function get_user_friends(int $id) : array 
    {
        global $wpdb;
        $tablename  = $wpdb->prefix.'peepso_friends_cache';

        $results = $wpdb->get_col("SELECT friend_id
            FROM $tablename 
            WHERE user_id = $id");

        return $results;
        wp_die();
    }

    public static function getInvalidVendors() : array 
    {
        $ids  = array();
        $args = array(
            'meta_key'   => 'clicks_pack_active',
            'meta_value' => '0'
        );
        $user_query = new WP_User_Query( $args );

        if ( ! empty( $user_query->get_results() ) ) {
            foreach ( $user_query->get_results() as $user ) {
                array_push($ids, $user->ID);
            }
        }

        return $ids;
    }

    public static function getValidVendors() : array 
    {
        $ids  = array();
        $args = array(
            'meta_key'   => 'clicks_pack_active',
            'meta_value' => '1'
        );
        $user_query = new WP_User_Query( $args );

        if ( ! empty( $user_query->get_results() ) ) {
            foreach ( $user_query->get_results() as $user ) {
                array_push($ids, $user->ID);
            }
        }

        return $ids;
    }

    public static function is_user_member( $user = null ) : bool
    {
        if( !is_user_logged_in() ){
            return false;
        }

        if( !$user ){
            $user = wp_get_current_user();
            $user_id = $user->ID;
        
        }elseif( $user instanceof WP_User ){
            $user_id = $user->ID;
        }

        return false;
    }


    public static function is_user_influencer( $user = null ) : bool
    {
        if(!is_user_logged_in( ))
        {
            return false;
        }

        if( !$user ){
            $user = wp_get_current_user();
            $user_id = $user->ID;
        
        }elseif( $user instanceof WP_User ){
            $user_id = $user->ID;
        
        }else{
            $user_id = $user;
        }

        $user_data = get_userdata( $user_id );

        return in_array( 'influencer', $user_data->roles );
    }

    public static function is_user_vendor( $user = null ) : bool
    {
        if(!is_user_logged_in( ))
        {
            return false;
        }

        if( !$user ){
            $user = wp_get_current_user();
            $user_id = $user->ID;
        
        }elseif( $user instanceof WP_User ){
            $user_id = $user->ID;
        
        }else{
            $user_id = $user;
        }
        
        $user_data = get_userdata( $user_id );

        return in_array( 'vendor', $user_data->roles );
    }

    public static function is_user_vendor_or_influencer( $user = null ){

        if( !$user ){
            $user = wp_get_current_user();
            $user_id = $user->ID;
        
        }elseif( $user instanceof WP_User ){
            $user_id = $user->ID;
        
        }else{
            $user_id = $user;
        }

        return self::is_user_influencer( $user ) || self::is_user_vendor( $user );

    }

    public static function get_profile_page_user(){

        $profile_user_id = PeepSoProfileShortcode::get_instance()->get_view_user_id();
        
        if( $profile_user_id === 0 ){
            return false;
        }

        return new WP_User( $profile_user_id );

    }


    public static function is_current_user_profile_page(){

        $profile_page_user = self::get_profile_page_user();
        
        if( $profile_page_user === false ) return false;

        $current_user = wp_get_current_user();
        
        return $profile_page_user->ID == $current_user->ID;
        
    }

    
  


    public static function twig_breadcrumb() : void
    {

        $sep = ' / ';
        $collection_page = btw_get_page_by_template( 'page-collections.php' );

        if (!is_front_page()) {
        
        // Start the breadcrumb with a link to your homepage
        echo '<div class="breadcrumbs">';
        echo '<a href="';
        echo is_tax( 'collection_categories' ) ? get_the_permalink( $collection_page ) : get_option('home');
        echo '">';
        echo is_tax( 'collection_categories' ) ? 'Twigs' : bloginfo('name');
        echo '</a>' . $sep;
    
        // Check if the current page is a category, an archive or a single page. If so show the category or archive name.
        if (is_category() || is_single() ){
            //the_category('title_li=');
        } 
        elseif (is_archive())
        {
            if ( is_day() ) {
                printf( __( '%s', 'text_domain' ), get_the_date() );
            } elseif ( is_month() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'F Y', 'monthly archives date format', 'text_domain' ) ) );
            } elseif ( is_year() ) {
                printf( __( '%s', 'text_domain' ), get_the_date( _x( 'Y', 'yearly archives date format', 'text_domain' ) ) );
            } else {
                $queried_object = get_queried_object();
                $tax            = get_taxonomy( $queried_object->taxonomy );
                $terms          = get_term_parents_list($queried_object->term_id, $tax->name, [ 'separator' => ' / ' ]);
                echo rtrim( $terms, ' / ' );
            }
        }
        
        // If the current page is a single post, show its title with the separator
        if (is_single()) {
            global $post;
            $terms      = get_the_terms( $post->ID, 'collection_categories' );
            $category   = !empty($terms) 
                          ? $terms[0]
                          : null;
            $categories = !is_null($category) 
                          ? get_term_parents_list($category->term_taxonomy_id, $category->taxonomy, [ 'separator' => ' / ' ])
                          : '';
            $post_title = get_the_title();

            echo rtrim( $categories, ' / ' ); // .''. $post_title;
        }
        
        // If the current page is a static page, show its title.
        if (is_page()) {
            echo the_title();
        }
        
        // if you have a static page assigned to be you posts list page. It will find the title of the static page and display it. i.e Home >> Blog
        if (is_home()){
            global $post;
            $page_for_posts_id = get_option('page_for_posts');
            if ( $page_for_posts_id ) { 
                $post = get_post($page_for_posts_id);
                setup_postdata($post);
                the_title();
                rewind_posts();
            }
        }

        echo '</div>';
        }
    }

}