<?php

/* The core plugin class.
 *
 *
 * @since      1.0.0
 * @package    Twigbe
 * @subpackage Plugin_Name/includes
 * @author     Sotiris Kyritsis, Krikor Boghossian
 */
class Twigbe {

    /**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Twigbe_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	public function __construct() 
	{

		if ( defined( 'PLUGIN_NAME_VERSION' ) ) {
			$this->version = PLUGIN_NAME_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'twigbe';
	}

	/**
	 * Registers the initial hooks to get the plugin going, if you're
	 * curious, see https://developer.wordpress.org/plugins/hooks/
	 *
	 * In short, both actions and filters are like events or callbacks. The difference
	 * between them is that the return value from filters is passed to the next callback,
	 * while the return value of actions is ignored. In this plugin we're using mostly actions.
	 */
	public function add_init_action() : void
	{

		/* a plugin shouldn't do anything before the "init" hook,
		 * that's why the main initialization code is in the init() method
		 */
		add_action( 'init', array( $this, 'init' ) );
	}

	/**
	 * Load all the classes responsible for the plugins' functionality
	 */
    private function load_dependencies() : void
	{

        /**
		 * The class responsible for managing requests from the FE
		 */
        require plugin_dir_path( __FILE__ ) . 'class-twigbe-ajax.php';

        /**
		 * The class responsible for autoposting to peepso
		 */
        require plugin_dir_path( __FILE__ ) . 'class-twigbe-post.php';

		/**
		 * The class responsible for the analytics
		 */
		require plugin_dir_path( __FILE__ ) . 'class-twigbe-clicks.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
        require plugin_dir_path( __FILE__ ) . 'class-twigbe-loader.php';

		/**
		 * The class responsible for the common stuff
		 */
		require plugin_dir_path( __FILE__ ) . 'class-twigbe-common.php';

		/**
		 * The class responsible for the administrator UI
		 */
		require plugin_dir_path( __FILE__ ) . 'class-twigbe-admin-ui.php';


		/**
		 * The class responsible for the point system
		 */
		require plugin_dir_path( __FILE__ ) . 'class-twigbe-points.php';

		$this->loader = new Twigbe_Loader();
	}

	/**
	 * Hooks and new class instantiations go here
	 */
    function init() : void
	{
        $this->load_dependencies();
        $this->loader->run();

		add_action('admin_menu', array($this, 'twigbe_setup_menu' ) );
		
		add_filter( 'wp_mail_from', function( $email ) {
			return 'noreply@twigbe.com';
		} );

		add_filter( 'wp_mail_from_name', function( $name ) {
			return 'Twigbe';
		} );

        $this->load_ajax();
		$this->load_autpost();
		$this->load_analytics();
		$this->load_point_system();

		$this->load_common_stuff();
		$this->load_admin_ui();
	}

    private function load_ajax() : void
	{
        $plugin_ajax = new Twigbe_Ajax( );
    }

	private function load_autpost() : void
	{
        $plugin_autopost = new Twigbe_AutoPost( );
    }

	private function load_analytics() : void
	{
        $plugin_analytics = new Twigbe_Click_Counter( );
    }

	private function load_common_stuff() : void
	{
        $common_shared = new Twigbe_Common( );
    }

	private function load_admin_ui() : void
	{
        $admin_ui = new Twigbe_Admin_UI( );
    }

	private function load_point_system() : void
	{
		$point_system = new Twigbe_Points_System( );
	}

	public function twigbe_setup_menu() : void
	{
        add_menu_page( 'Twigbe Plugin Page', 'Twigbe Settings', 'edit_theme_options', 'twigbe-admin', array($this, 'twigbe_admin_init' ));
    }

    /**
     * Admin Page Layout
	 * Basic options for now
     */
    public function twigbe_admin_init() : void
	{
		$admin_ui = new Twigbe_Admin_UI( );
		$admin_ui->render();
    }
}